// Name        : GrassBot Final Code
// Authors     : AS2017911 - K.D. Ekanayaka
//               AS2017934 - M.T. Mahalekamge
//               AS2017949 - I.M.K Soysa
// Version     : 9.2
// Description : Grassbot is a Smart grass cutter gets lawn boundaries as inputs from its mobile app.


#include "StringSplitter.h"

float lengthOfLawn; // Length variable from the mobile app in cm
float widthOfLawn; // Width variable from the mobile app in cm



// Begin Motor ports define--------------------------------------------------------------------------------

const int motorPin1  = 9;
const int motorPin2  = 10;

const int motorPin3  = 6;
const int motorPin4  = 5;

const int bladeMotorPin = 8;

// End Motor ports------------------------------------------------------------------------------------------



bool wait; // Variable that used for create a wait until GrassBot received the lawn information
String incomingMessage; // Variable that stores message that includes lawn information coming from the mobile app

const float gbSpeed = 62.11; // Grassbot's speed; units = cm/s

const int gbWidth = 15; // Grassbot's width in cm



// Begin Ultrasonic sensor ports-----------------------------------------------------------------------------

const int trigPin = 11;
const int echoPin = 3;

// End Ultrasonic sensor ports-------------------------------------------------------------------------------




float runTime; // Units = seconds
float dTime; // Disturbing Time in Seconds

int nN; // Number of iterations needs to go

void setup() {

  Serial.begin(9600);

  //Set pins as outputs
  pinMode(motorPin1, OUTPUT);
  pinMode(motorPin2, OUTPUT);
  pinMode(motorPin3, OUTPUT);
  pinMode(motorPin4, OUTPUT);

  pinMode(bladeMotorPin, OUTPUT);

  pinMode(trigPin, OUTPUT); // trig pin will have pulses output
  pinMode(echoPin, INPUT); // echo pin should be input to get pulse width

  do {
    if (Serial.available() > 0) {
      incomingMessage = Serial.readString();
      Serial.println("Incoming Message: " + incomingMessage);

      StringSplitter *splitter = new StringSplitter(incomingMessage, ' ', 2);

      lengthOfLawn = splitter->getItemAtIndex(0).toFloat() * 100;
      widthOfLawn = splitter->getItemAtIndex(1).toFloat() * 100;

      Serial.print("Length: ");
      Serial.println(lengthOfLawn);

      Serial.print("Width: ");
      Serial.println(widthOfLawn);

      wait = false;
    }
    else {
      wait = true;
      Serial.println("In Loop");
      delay(500);
    }
  } while (wait);

  runTime = (lengthOfLawn / gbSpeed) * 1000;
  nN = ( widthOfLawn / gbWidth);

  digitalWrite(bladeMotorPin, HIGH);

  for (int n = 1; n <= nN; n++) {

    Serial.println(n);
    // Duration will be the input pulse width and distance will be the distance to the obstacle in centimeters
    long duration, distance;

    // Output pulse with 1ms width on trigPin
    digitalWrite(trigPin, HIGH);
    delay(1);
    digitalWrite(trigPin, LOW);

    // Measure the pulse input in echo pin
    duration = pulseIn(echoPin, HIGH);

    // Distance is half the duration devided by 29.1 (from datasheet)
    distance = (duration / 2) / 29.1;

    if (distance < lengthOfLawn) {

      Serial.println("Obstacle found");
      Serial.print("Distance ");
      Serial.println(distance);

      dTime = (distance / gbSpeed) * 1000;
      Serial.print("dTime ");
      Serial.println(dTime);

      motorRelease();

      Serial.println(dTime);
      if (dTime <= 100) {
        forward(1);
        dTime = 0;
      } else {
        forward(dTime - 100);
      }

      Serial.println("1st forward end");

      if (n % 2 == 1) {
        turnRight();

        forward(500);

        turnLeft();

        if ((runTime - dTime) <= 1000) {
          forward(runTime - dTime);

          turnRight();

          turnRight();

        } else {
          forward(1000);

          turnLeft();

          forward(500);

          turnRight();


          //        Serial.println("2nd forward begin");
          //        Serial.print("runTime - (dTime + 750) = ");
          //        Serial.println(runTime - (dTime + 750));

          forward(runTime - (dTime + 700));

          //        Serial.println("2nd forward end");
          //        Serial.println(runTime);
          //        Serial.println("code ends");

          turnRight();

          forward(500);

          turnRight();

        }
      } else {
        turnLeft();

        forward(500);

        turnRight();

        if ((runTime - dTime) <= 1000) {
          forward(runTime - dTime);

          turnLeft();

          turnLeft();

        } else {
          forward(1000);

          turnRight();

          forward(500);

          turnLeft();

          forward(runTime - (dTime + 1000));

          turnLeft();

          forward(500);

          turnLeft();

        }
      }
    }
    else if (distance > lengthOfLawn && n % 2 == 1) {

      //      Serial.println("Obstacle does not found 1");

      motorRelease();

      forward(runTime);

      turnRight();

      forward(500);

      turnRight();

    }
    else if (distance > lengthOfLawn && n % 2 == 0) {

      //      Serial.println("Obstacle does not found 2");

      motorRelease();

      forward(runTime);

      turnLeft();

      forward(500);

      turnLeft();

    }
  }
  motorRelease();
}

void loop() {

}

// Begin Motor functioning functions---------------------------------------------------------

void forward(int runningTime) {
  analogWrite(motorPin1, 180);
  analogWrite(motorPin2, 0);
  analogWrite(motorPin3, 0);
  analogWrite(motorPin4, 180);

  delay(runningTime);

  motorRelease();
}

void turnRight() {
  analogWrite(motorPin1, 180);
  analogWrite(motorPin2, 0);
  analogWrite(motorPin3, 180);
  analogWrite(motorPin4, 0);

  delay(580);

  motorRelease();
}

void turnLeft() {
  analogWrite(motorPin1, 0);
  analogWrite(motorPin2, 180);
  analogWrite(motorPin3, 0);
  analogWrite(motorPin4, 180);

  delay(580);

  motorRelease();
}

void motorRelease() {
  analogWrite(motorPin1, 0);
  analogWrite(motorPin2, 0);
  analogWrite(motorPin3, 0);
  analogWrite(motorPin4, 0);

  delay(500);
}

// End Motor functioning functions-------------------------------------------------------
