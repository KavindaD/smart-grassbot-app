import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  
  {
    path: 'aboutUs',
    loadChildren: () => import('./home/about-us/about-us.module').then( m => m.AboutUsPageModule)
  },
  {
    path: 'instructions',
    loadChildren: () => import('./home/instructions/instructions.module').then( m => m.InstructionsPageModule)
  },
  {
    path: 'mowing',
    loadChildren: () => import('./home/mowing/mowing.module').then( m => m.MowingPageModule)
  },
  {
    path: 'location',
    loadChildren: () => import('./home/location/location.module').then( m => m.LocationPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
