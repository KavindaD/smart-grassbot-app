import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MowingPage } from './mowing.page';

const routes: Routes = [
  {
    path: '',
    component: MowingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MowingPageRoutingModule {}
