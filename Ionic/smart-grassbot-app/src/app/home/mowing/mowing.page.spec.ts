import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MowingPage } from './mowing.page';

describe('MowingPage', () => {
  let component: MowingPage;
  let fixture: ComponentFixture<MowingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MowingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MowingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
