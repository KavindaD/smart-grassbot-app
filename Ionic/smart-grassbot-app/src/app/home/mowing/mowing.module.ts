import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MowingPageRoutingModule } from './mowing-routing.module';

import { MowingPage } from './mowing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MowingPageRoutingModule
  ],
  declarations: [MowingPage]
})
export class MowingPageModule {}
