import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MowingPage } from '../mowing/mowing.page';



@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {

  lawn = {
    lengthOfLawn: null,
    widthOfLawn: null
  };

  constructor( private router: Router ) { }

  ngOnInit() {
  }

  openDetailsWithState() {
    let navigationExtras: NavigationExtras = {
      state: {
        lawn: this.lawn
      }
    }
    this.router.navigate(['mowing'],navigationExtras);
  }

}
